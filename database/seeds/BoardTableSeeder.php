<?php

use Illuminate\Database\Seeder;

class BoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $board=new \App\Board();
        $board->title='administration';
        $board->image='';
        $board->save();

        $board=new \App\Board();
        $board->title='departments';
        $board->image='';
        $board->save();


        $board=new \App\Board();
        $board->title='entertainment';
        $board->image='';
        $board->save();


        $board=new \App\Board();
        $board->title='psu';
        $board->image='';
        $board->save();


        $board=new \App\Board();
        $board->title='religious';
        $board->image='';
        $board->save();


        $board=new \App\Board();
        $board->title='sports';
        $board->image='';
        $board->save();

    }
}
