<?php

use Illuminate\Database\Seeder;
use App\Program;

class ProgramTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $program=new Program();
        $program->name='Bachelor of Electrical and Electronics Engineering';
        $program->code='beee';
        $program->year='5';
        $program->save();

        $program=new Program();
        $program->name='Bachelor of Electronics and Computer Engineering';
        $program->code='bece';
        $program->year='5';
        $program->save();

        $program=new Program();
        $program->name='Bachelor of Biomedical Engineering';
        $program->code='bbme';
        $program->year='5';
        $program->save();
    }
}
