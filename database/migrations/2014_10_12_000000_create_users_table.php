<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('google_id')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('program_id')->unsigned()->nullable();
            $table->integer('year')->nullable();
            $table->integer('account_number')->nullable();
            $table->text('avatar');
            $table->text('google_token');
            $table->text('app_access_token')->nullable();
            $table->text('web_access_token')->nullable();
            $table->boolean('notification');
            $table->boolean('auto_download_images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
