<?php


Route::group(['prefix' => 'v1'], function(){

    Route::group([
        'prefix' => 'user'
    ], function () {
        /*
        |-------------------------------------------------------------------------------
        | Logs in user to google
        |-------------------------------------------------------------------------------
        | URL:            /api/v1/user/login/google
        | Method:         GET
        | Controller:     API\GoogleAuthenticationController
        | Description:    Redirects to google sign in
        */
//        Route::get('/login/google', 'API\GoogleAuthenticationController@getSocialRedirect');
        /*
        |-------------------------------------------------------------------------------
        | Checks whether or not a user exists in the database
        |-------------------------------------------------------------------------------
        | URL:            /api/v1/user/authenticate/{google_id}
        | Method:         GET
        | Controller:     API\UserController
        | Description:    Returns a true if user exists and false if not
        | Parameters:
        |   google_id -> ID of the user being authenticated
        */
//        Route::get('/authenticate/{google_id}','API\UserController@getAuthenticateUser');
        /*
        |-------------------------------------------------------------------------------
        | Adds user to the database
        |-------------------------------------------------------------------------------
        | URL:            /api/v1/user/add-to-database
        | Controller:     API\UserController
        | Method:         POST
        | Description:    Adds authenticated user to the database
        | Body (all required):
        |   google_id, google_token, name, email, avatar
        */

        Route::post('/login','API\UserController@postLogin');
        /*
         |-------------------------------------------------------------------------------
         | Gets a new token for a user
         |-------------------------------------------------------------------------------
         | URL:            /api/v1/user/new-token/{google_id}
         | Controller:     API\UserController
         | Method:         GET
         | Description:    Returns the user with the new token
         | Parameters:
         |   google_id -> ID of the user being authenticated
         */

//        Route::get('new-token/{google_id}', 'API\UserController@getNewToken');

        Route::group(['middleware' => ['jwt.verify']], function() {
            /*
            |-------------------------------------------------------------------------------
            | Gets the authenticated user
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/user
            | Method:         GET
            | Controller:     API\UserController
            | Middleware:     JWTAuth (requires token)
            | Description:    Returns a user object
            */
            Route::get('/', 'API\UserController@getAuthenticatedUser');
            /*
            |-------------------------------------------------------------------------------
            | Updates the user information
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/user
            | Method:         POST
            | Controller:     API\UserController
            | Middleware:     JWTAuth (requires token)
            | Description:    Returns a user object
            */
            Route::post('/', 'API\UserController@postUpdateUser');

            Route::post('/search','API\UserController@search');
        });

    });


   Route::group([
        'middleware' => ['jwt.verify','roles']
    ], function () {
       /*
        |-------------------------------------------------------------------------------
        | Gets all the notices
        |-------------------------------------------------------------------------------
        | URL:            /api/v1/notices/get/{timestamp}
        | Controller:     API\NoticeController
        | Method:         GET
        | Middleware      JWTAuth (requires token), Roles
        | Allowed Roles:  *
        | Description:    Returns notices posted after the timestamp
        | Parameters:
        |   timestamp-> last time a user synced data
        */
        Route::get('/get/{timestamp}','API\AppController@getAppUpdates');

       /*
       |-------------------------------------------------------------------------------
       | Sends feedback to the poly noticeboard contact mail
       |-------------------------------------------------------------------------------
       | URL:            /api/v1/feedback
       | Controller:     API\AppController
       | Method:         POST
       | Middleware      JWTAuth (requires token), Roles
       | Allowed Roles:  *
       | Description:    Returns true or false
       */
        Route::post('/feedback','API\AppController@postFeedback');

        Route::group([
            'prefix' => 'notices'
        ], function () {
            /*
            |-------------------------------------------------------------------------------
            | Gets all the notices
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices
            | Controller:     API\NoticeController
            | Method:         GET
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  *
            | Description:    Returns all notices
            */
            Route::get('/','API\NoticeController@getNotices');
            /*
            |-------------------------------------------------------------------------------
            | Gets a notice
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices/{slug}
            | Controller:     API\NoticeController
            | Method:         GET
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  *
            | Description:    Returns single notice
            | Parameters:
            |   slug -> slug of the notice
            */
            Route::get('/{slug}','API\NoticeController@getNotice');
            /*
            |-------------------------------------------------------------------------------
            | Adds notice to the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices/new
            | Controller:     API\NoticeController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the created notice
            | Body (all required):
            |   title, body, board_id (int)
            */
            Route::post('/new',[
                'uses'  =>'API\NoticeController@postNewNotice',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Adds notice to the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices/new
            | Controller:     API\NoticeController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the created notice
            | Body (all required):
            |   title, body, board_id (int)
            */
            Route::post('/update/{slug}',[
                'uses'  =>'API\NoticeController@postUpdateNotice',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Soft deletes a notice in the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices/{slug}
            | Controller:     API\NoticeController
            | Method:         DELETE
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns a true if successful false otherwise
            | Parameters:
            |   slug -> slug of the notice
            */
            Route::delete('/{slug}',[
                'uses'  =>'API\NoticeController@deleteNotice',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Displays only soft deleted notices
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices/trashed
            | Controller:     API\NoticeController
            | Method:         DELETE
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns a true if successful false otherwise
            */
            Route::get('/trash/trashed',[
                'uses'  =>'API\NoticeController@getTrashedNotices',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Permanently deletes a notice in the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices/{slug}/trash
            | Controller:     API\NoticeController
            | Method:         DELETE
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns a true if successful false otherwise
            | Parameters:
            |   slug -> slug of the notice
            */
            Route::delete('/trash/{slug}',[
                'uses'  =>'API\NoticeController@deleteTrashNotice',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Restores a soft deleted notice in the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/notices/{slug}/restore
            | Controller:     API\NoticeController
            | Method:         DELETE
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns a true if successful false otherwise
            | Parameters:
            |   slug -> slug of the notice
            */
            Route::get('/trash/trashed/{slug}',[
                'uses'  =>'API\NoticeController@getRestoreNotice',
                'roles' =>['developer','admin','author']
            ]);
        });

        Route::group([
            'prefix' => 'boards'
        ], function () {
            /*
            |-------------------------------------------------------------------------------
            | Gets all the boards
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/boards
            | Controller:     API\BoardController
            | Method:         GET
            | Middleware      JWTAuth (requires token)
            | Description:    Returns a list of the boards
            */
            Route::get('/','API\BoardController@getBoards');
            /*
            |-------------------------------------------------------------------------------
            | Gets a board
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/boards/{title}
            | Controller:     API\BoardController
            | Method:         GET
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  *
            | Description:    Retuns a board with its notices
            | Parameters:
            |   title -> title of the board
            */
            Route::get('/{title}','API\BoardController@getBoard');
            /*
            |-------------------------------------------------------------------------------
            | Adds board to the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/boards/new
            | Controller:     API\BoardController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  *
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the created notice
            | Body (all required):
            |   title, image
            */
            Route::post('/new',[
                'uses'  =>'API\BoardController@postNewBoard',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Updates a board
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/boards/{title}
            | Controller:     API\BoardController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the updated board
            | Parameters:
            |   title -> title of the board
            | Body (all required):
            |   title, image
            */
            Route::post('/{title}',[
                'uses'  =>'API\BoardController@postUpdateBoard',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Deletes a board in the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/board/{title}
            | Controller:     API\BoardController
            | Method:         DELETE
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns a true if successful false otherwise
            | Parameters:
            |   title -> title of the board
            */
            Route::delete('/{title}',[
                'uses'  =>'API\BoardController@deleteBoard',
                'roles' =>['developer','admin','author']
            ]);
        });

        Route::group([
            'prefix' => 'programs'
        ], function () {
            /*
            |-------------------------------------------------------------------------------
            | Gets all the programs
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/programs
            | Controller:     API\ProgramController
            | Method:         GET
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  *
            | Description:    Returns a list of the programs
            */
            Route::get('/','API\ProgramController@getPrograms');
            /*
             |-------------------------------------------------------------------------------
             | Gets a program
             |-------------------------------------------------------------------------------
             | URL:            /api/v1/programs/{code}
             | Controller:     API\ProgramController
             | Method:         GET
             | Middleware      JWTAuth (requires token), Roles
             | Allowed Roles:  *
             | Description:    Retuns a program with its students
             | Parameters:
             |   code -> code of the board
             */
            Route::get('/{code}','API\ProgramController@getProgram');
            /*
            |-------------------------------------------------------------------------------
            | Adds program to the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/programs/new
            | Controller:     API\ProgramController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the created program
            | Body (all required):
            |   name, code, year (int)
            */
            Route::post('/new',[
                'uses'  =>'API\ProgramController@postNewProgram',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Updates a program
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/programs/{code}
            | Controller:     API\ProgramController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the updated program
            | Parameters:
            |   code -> code of the board
            | Body (all required):
            |   name, code, year (int)
            */
            Route::post('/{code}',[
                'uses'  =>'API\ProgramController@postUpdateProgram',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Deletes a program in the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/programs/{code}
            | Controller:     API\ProgramController
            | Method:         DELETE
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns a true if successful false otherwise
            | Parameters:
            |   code -> code of the board
            */
            Route::delete('/{code}',[
                'uses'  =>'API\ProgramController@deleteProgram',
                'roles' =>['developer','admin','author']
            ]);
        });

        Route::group([
            'prefix' => 'admin',
        ], function () {
            /*
            |-------------------------------------------------------------------------------
            | Assigns Admin role to a user
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/admin/assign-admin-role
            | Controller:     API\UserController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Assigns or removes a user as admin and returns the user object
            | Body (all required):
            |   google_id, admin (boolean)
            */
            Route::post('/assign-role',[
                'uses'  =>'API\UserController@postAssignAdminRole',
                'roles' =>['developer','admin']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Hands over admin role to a user
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/admin/handover-role
            | Controller:     API\UserController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Admin
            | Description:
            */
            Route::post('/handover-role',[
                'uses'  =>'API\UserController@postHandoverAdminRole',
                'roles' =>['admin']
            ]);

        });

        Route::group([
            'prefix' => 'polls',
        ], function () {
            /*
           |-------------------------------------------------------------------------------
           | Gets all the polls
           |-------------------------------------------------------------------------------
           | URL:            /api/v1/polls
           | Controller:     API\PollController
           | Method:         GET
           | Middleware      JWTAuth (requires token), Roles
           | Allowed Roles:  *
           | Description:    Returns a list of the polls
           */
            Route::get('/','API\PollController@index');

            /*
           |-------------------------------------------------------------------------------
           | Get a poll
           |-------------------------------------------------------------------------------
           | URL:            /api/v1/polls/{id}
           | Controller:     API\PollController
           | Method:         GET
           | Middleware      JWTAuth (requires token), Roles
           | Allowed Roles:  *
           | Description:    Returns a poll
           */
            Route::get('/{id}','API\PollController@getPoll');

            /*
            |-------------------------------------------------------------------------------
            | Adds poll to the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/polls/new
            | Controller:     API\PollController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the created poll
            | Body (all required):
            |   title, description, options[]
            */
            Route::post('/new',[
                'uses'  =>'API\PollController@store',
                'roles' =>['developer','admin','author']
            ]);
            /*
            |-------------------------------------------------------------------------------
            | Updates a poll
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/polls/{code}
            | Controller:     API\PollController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the updated program
            | Parameters:
            |   id -> id of the poll
            | Body (all required):
            |   title, description, options[]
            */
//            Route::post('/{id}',[
//                'uses'  =>'API\PollController@update',
//                'roles' =>['developer','admin']
//            ]);
            /*
            |-------------------------------------------------------------------------------
            | Deletes a poll in the database
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/polls/{code}
            | Controller:     API\PollController
            | Method:         DELETE
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns a true if successful false otherwise
            | Parameters:
            |   id -> id of the poll
            */
            Route::post('/delete/{id}',[
                'uses'  =>'API\PollController@deletePoll',
                'roles' =>['developer','admin','author']
            ]);

            /*
            |-------------------------------------------------------------------------------
            | Submits a vote to a poll
            |-------------------------------------------------------------------------------
            | URL:            /api/v1/polls/vote
            | Controller:     API\PollController
            | Method:         POST
            | Middleware      JWTAuth (requires token), Roles
            | Allowed Roles:  Developer, Admin
            | Description:    Returns the updated poll
            | Body (all required):
            |   poll_id, google_id, option_id
            */
            Route::post('/vote',[
                'uses'  =>'API\PollController@postVote',
            ]);

        });

       Route::group([
//           'prefix' => 'subscriptions',
       ], function () {
           /*
          |-------------------------------------------------------------------------------
          | Gets all the user's subscribed boards
          |-------------------------------------------------------------------------------
          | URL:            /api/v1/subscriptions
          | Controller:     API\UserController
          | Method:         GET
          | Middleware      JWTAuth (requires token), Roles
          | Allowed Roles:  *
          | Description:    Returns a list of the subscribed boards
          */
           Route::get('/subscriptions','API\UserController@getSubscriptions');
           /*
          |-------------------------------------------------------------------------------
          | Gets all the user's settings
          |-------------------------------------------------------------------------------
          | URL:            /api/v1/settings
          | Controller:     API\UserController
          | Method:         GET
          | Middleware      JWTAuth (requires token), Roles
          | Allowed Roles:  *
          | Description:    Returns a list of the settings
          */
           Route::get('/settings','API\UserController@getSettings');

           /*
          |-------------------------------------------------------------------------------
          | Manages subscribed boards
          |-------------------------------------------------------------------------------
          | URL:            /api/v1/subscriptions
          | Controller:     API\UserController
          | Method:         POST
          | Middleware      JWTAuth (requires token), Roles
          | Allowed Roles:  *
          | Description:    Returns a true if successfully managed the boards
          | Body:
          |   array of subscribed_boards[boards_ids], array of unsubscribed_boards[boards_ids]
          */
           Route::post('/settings','API\UserController@postSettings');

       });

    });
});