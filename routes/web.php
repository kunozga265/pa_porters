<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'API\AppController@getApp');

//Route::get('/logout', 'Web\AppController@getLogout')
//    ->middleware('auth');
//
//Route::get('/login', 'Web\AppController@getLogin')
//    ->name('login')
//    ->middleware('guest');

//Route::get('/login/google', 'Web\GoogleAuthenticationController@getSocialRedirect')
//    ->middleware('guest');

//Route::get('/login/google/callback', 'API\GoogleAuthenticationController@getSocialCallback');

