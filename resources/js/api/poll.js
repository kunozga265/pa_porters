/*
    Imports the Roast API URL from the config.
*/
import { POLYNOTICEBOARD_CONFIG } from '../config.js';

export default {
    /*
        GET
        /api/v1/polls
    */
    getPolls: function(page,token){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/polls?page='+page,{headers:{"Authorization":"Bearer "+token}});
    },

    /*
      GET
      /api/v1/polls/{id}
    */
    getPoll: function( id,token){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/polls/' + id,{headers:{"Authorization":"Bearer "+token}});
    },

    /*
      POST
      /api/v1/polls
    */
    postAddNewPoll: function( title, description, options, start_date,end_date,type, token){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/polls/new',
            {
                title: title,
                description: description,
                options: options,
                start_date: start_date,
                end_date: end_date,
                type: type,
            },
            {
                headers:{
                    "Authorization":"Bearer "+token
                }
            }
        );
    },

    /*
      POST
      /api/v1/polls/vote
    */
    postVote: function(option_id,token){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/polls/vote',
            {
                option_id: option_id,
            },
            {
                headers:{
                    "Authorization":"Bearer "+token
                }
            }
        );
    },
    /*
      DELETE
      /api/v1/polls/{id}
    */
    deletePoll: function(id,token){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/polls/delete/'+id,
            {
                id: id,
            },
            {
                headers:{
                    "Authorization":"Bearer "+token
                }
            }
        );
    },



}