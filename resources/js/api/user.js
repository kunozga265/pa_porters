import { POLYNOTICEBOARD_CONFIG } from '../config.js';

export default{
    /*
        GET
        /api/v1/user/add-to-database
    */
    postLogin: function(google_id,name,email,avatar,google_token){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/user/login',{
            google_id:google_id,
            name:name,
            email:email,
            avatar:avatar,
            google_token:google_token
        }, {
            headers:{"Authorization":"Web"
            }
        });
    },
    /*
        GET
        /api/v1/user/
    */
    // getUser: function(google_id){
    //
    //     return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/user/authenticate/'+google_id);
    // },
    /*
        POST
        /api/v1/user
    */
    postUpdateUser: function(code,year,account_number,token){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/user',{
            code:code,
            year:year,
            account_number:account_number
        }, {
            headers:{"Authorization":"Bearer "+token
            }
        });
    },
    /*
        POST
        /api/v1/user
    */
    updateRole: function(data){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/admin/assign-role',{
            google_id:data.google_id,
            admin:data.admin,
            author:data.author,
        }, {
            headers:{"Authorization":"Bearer "+data.accessToken
            }
        });
    },

    /*
        POST
        /api/v1/user/search
    */
    getUsers:function (text,token) {
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/user/search',{
            text:text,
        }, {
            headers:{"Authorization":"Bearer "+token
            }
        });
    },

    /*
        GET
        /api/v1/user/new-token
    */
    getToken: function (google_id) {
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/user/new-token/'+ google_id);
    }
}