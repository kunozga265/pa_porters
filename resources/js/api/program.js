/*
    Imports the Roast API URL from the config.
*/
import { POLYNOTICEBOARD_CONFIG } from '../config.js';

export default {
    /*
        GET
        /api/v1/programs
    */
    getPrograms: function(token){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/programs',{headers:{"Authorization":"Bearer "+token}});
    },

    /*
      GET
      /api/v1/programs/{code}
    */
    getProgram: function( code,token ){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/programs/' + code,{headers:{"Authorization":"Bearer "+token}});
    },

    /*
      POST
      /api/v1/programs
    */
    // postAddNewNotice: function( title, body, board_id, image, token){
    //     return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/notices',
    //         {
    //             title: title,
    //             body: body,
    //             board_id: board_id,
    //             image: image,
    //         },
    //         {
    //             headers:{
    //                 "Authorization":"Bearer "+token
    //             }
    //         }
    //     );
    // }

}