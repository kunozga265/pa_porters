/*
    Imports the Roast API URL from the config.
*/
import { POLYNOTICEBOARD_CONFIG } from '../config.js';

export default {
    /*
        GET
        /api/v1/notices
    */
    getNotices: function(page, title,token){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/boards/'+ title +'?page='+page,{headers:{"Authorization":"Bearer "+token}});
    },
    /*
      GET
      /api/v1/notices/{noticeSlug}
    */
    getNotice: function( slug,token){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/notices/' + slug,{headers:{"Authorization":"Bearer "+token}});
    },

    /*
      POST
      /api/v1/notices
    */
    postAddNewNotice: function( title, body, board_id, featured, image, token){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/notices/new',
            {
                title: title,
                body: body,
                board_id: board_id,
                featured: featured,
                image: image,
                
            },
            {
                headers:{
                    "Authorization":"Bearer "+token
                }
            }
        );
    },
    update: function(data){
        return axios.post( POLYNOTICEBOARD_CONFIG.API_URL + '/notices/update/' + data.slug,
            {
                title: data.title,
                body: data.body,
                board_id: data.board_id,
                featured: data.featured,
                image: data.image,

            },
            {
                headers:{
                    "Authorization":"Bearer "+data.accessToken
                }
            }
        );
    },
    destroy: function(data){
        return axios.delete( POLYNOTICEBOARD_CONFIG.API_URL + '/notices/'+data.slug,
            {
                headers:{
                    "Authorization":"Bearer "+ data.accessToken
                }
            });
    },
    /*
        GET
        /api/v1/notices/trashed
    */
    getTrashedNotices: function(page, data){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/notices/trash/trashed'+'?page='+page,{headers:{"Authorization":"Bearer "+data.accessToken}});
    },
    restore: function(data){
        return axios.get( POLYNOTICEBOARD_CONFIG.API_URL + '/notices/trash/trashed/'+data.slug,
            {
                headers:{
                    "Authorization":"Bearer "+ data.accessToken
                }
            });
    },
    trash: function(data){
        return axios.delete( POLYNOTICEBOARD_CONFIG.API_URL + '/notices/trash/'+data.slug,
            {
                headers:{
                    "Authorization":"Bearer "+ data.accessToken
                }
            });
    },

}