/*
  Adds the promise polyfill for IE 11
*/
require('es6-promise').polyfill();

/*
    Imports Vue and Vuex
*/
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

/*
    Initializes Vuex on Vue.
*/
Vue.use( Vuex );

//Sets up Vuex Persist
// const vuexLocalStorage=new VuexPersist({
//     key:        'vuex', //The key to store the state on in the storage provider.
//     storage:    window.localStorage, //or window.sessionStorage or localForage
//
//     //Function that passes the state and returns the state with only the objects you want to store
//     // reducer:state=>state,
//     reducer:state=>({
//         user:state.user,
//         boards:state.boards,
//     }),
//     //Function that passes a mutation and lets you decide if it should update the state in localStorage
//     filter:   mutation=> (true)
// });

/*
    Imports all of the modules used in the application to build the data store.
*/
import { notices } from './modules/notices.js'
import { programs } from './modules/programs.js'
import { boards } from './modules/boards.js'
import { polls } from './modules/polls.js'
import { user } from './modules/user.js'


/*
  Exports our data store.
*/
export default new Vuex.Store({
    modules: {
        notices,
        user,
        programs,
        boards,
        polls
    },
    // plugins:[vuexLocalStorage.plugin]
});