/*
|-------------------------------------------------------------------------------
| routes.js
|-------------------------------------------------------------------------------
| Contains all of the routes for the application
*/

/*
    Imports Vue and VueRouter to extend with the routes.
*/
import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from './pages/Layout.vue';
import User from './pages/Users.vue';
import SignIn from './pages/SignIn.vue';
import Home from './pages/Home.vue';
import Boards from './pages/Boards.vue';
import Polls from './pages/Polls.vue';
import NewPoll from './pages/NewPoll.vue';
import NewNotice from './pages/NewNotice.vue';
import UpdateNotice from './pages/UpdateNotice.vue';
import TrashedNotice from './pages/TrashedNotice.vue';

/*
    Extends Vue to use Vue Router
*/
Vue.use( VueRouter );

//home
//notices -> All notices
//notices/new -> Add Notice
//notices/:id -> Display Individual Cafe

/*
    Makes a new VueRouter that we will use to run all of the routes
    for the app.
*/

export default new VueRouter({
    routes: [
        {
            path: '/',
            // name: 'layout',
            component: Layout,
            children: [
                {
                    path: '/',
                    name: 'home',
                    // component: Home
                    component: Home
                },
                {
                    path: '/user',
                    name: 'user',
                    // component: Home
                    component: User,
                    props:{id:true},
                    meta:{
                        auth:true
                    }
                },
                {
                    path: '/sign-in',
                    name: 'sign-in',
                    // component: Home
                    component: SignIn,
                    meta:{
                        guest:true
                    }
                },
                // {
                //     path: 'notices',
                //     name: 'notices',
                //     component: Notices,
                //     meta:{
                //         auth:true
                //     }
                // },
                {
                    path: 'notices/create',
                    name: 'create-notice',
                    component: NewNotice,
                    meta:{
                        auth:true,
                        admin:true,
                        author:true,
                    }
                },
                {
                    path: 'notices/update/:slug',
                    name: 'update-notice',
                    component: UpdateNotice,
                    props:true,
                    meta:{
                        auth:true,
                        admin:true,
                        author:true,
                    }
                },
                {
                    path: 'notices/trash',
                    name: 'trash',
                    component: TrashedNotice,
                    meta:{
                        auth:true,
                        admin:true,
                        author:true,
                    }
                },
                {
                    path: 'polls',
                    name: 'polls',
                    component: Polls,
                    meta:{
                        auth:true
                    }
                },
                // {
                //     path: 'polls/:id',
                //     name: 'poll',
                //     component: Poll,
                //     props:true,
                //     meta:{
                //         auth:true
                //     }
                // },
                {
                    path: 'polls/create',
                    name: 'create-poll',
                    component: NewPoll,
                    meta:{
                        admin:true,
                        author:true,
                        auth:true
                    }
                },
                {
                    path: 'boards',
                    name: 'boards',
                    component: Boards,
                    meta:{
                        auth:true
                    }
                },
                // {
                //     path: 'boards/:title',
                //     name: 'board',
                //     component: Board,
                //     props:true,
                //     meta:{
                //         auth:true
                //     }
                // },
                // {
                //     path: 'boards/:title/:slug',
                //     name: 'notice',
                //     component: Notice,
                //     props:true,
                //     meta:{
                //         auth:true
                //     }
                // },
                // {
                //     path: 'notices/:slug',
                //     name: 'notice',
                //     component: Vue.component( 'Notice', require( './pages/Notice.vue' ) ),
                //      meta:{
                //         auth:true
                //     }
                // }
            ]
        }
    ]
});
