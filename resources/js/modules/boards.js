/*
|-------------------------------------------------------------------------------
| VUEX modules/Boards.js
|-------------------------------------------------------------------------------
| The Vuex data store for the Boards
*/

import BoardAPI from '../api/board.js';

export const boards = {
    state: {
        boards:[],
        boardsLoadStatus:0,

        // program:{},
        // programLoadStatus:0,
    },
    actions:{
            loadBoards({commit},data){
            commit('setBoardsLoadStatus',1);

            if(data.accessToken!==''){
                BoardAPI.getBoards(data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setBoards',response.data);
                            commit('setBoardsLoadStatus',4);
                        }
                        else{
                            commit('setBoards',response.data.boards.data);
                            commit('setBoardsLoadStatus',2);
                        }

                    })
                    .catch(function (response) {
                        // commit('setBoards',response.message);
                        commit('setBoardsLoadStatus',3);
                    });
            }

        },

        clearBoards({commit}){
                commit('setBoards',[]);
                commit('setBoardsLoadStatus',0);
        }
    },
    mutations:{
        setBoards(state,boards){
            state.boards=boards;
        },
        setBoardsLoadStatus(state,status){
            state.boardsLoadStatus=status;
        },
    },
    getters:{
        getBoards(state){
            return state.boards;
        },
        getBoardsLoadStatus(state){
            return state.boardsLoadStatus;
        },
    }

};