/*
|-------------------------------------------------------------------------------
| VUEX modules/notices.js
|-------------------------------------------------------------------------------
| The Vuex data store for the notices
*/

import NoticeAPI from '../api/notice.js';

export const notices = {
    state: {
        board:{},
        notices:[],
        noticesLoadStatus:0,
        lastPage:0,
        currentPage:1,

        notice:{},
        noticeLoadStatus:0,
        noticeUpdateStatus:0,
        noticeDeleteStatus:0,

        //trashed notices
        trashedNotices:[],
        trashedNoticesLoadStatus:0,
    },
    actions:{
        loadNotices({commit},data){
            commit('setNoticesLoadStatus',1);

            if(data.accessToken!==''){
                NoticeAPI.getNotices(data.page, data.title, data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setNotices',response.data);
                            commit('setNoticesLoadStatus',4);
                        }
                        else{
                            commit('setNoticesLoadStatus',2);
                            commit('setNotices',response.data.notices.data);
                            commit('setLastPage',response.data.notices.meta.last_page);
                            commit('setCurrentPage',response.data.notices.meta.current_page);
                        }

                    })
                    .catch(function () {
                        commit('setNotices',[]);
                        commit('setNoticesLoadStatus',3);
                    });
            }

        },
        appendNotices({commit,state},data){
            commit('setNoticesLoadStatus',1);

            if(data.accessToken!=='' && state.currentPage<state.lastPage){
                let page=state.currentPage+1;
                NoticeAPI.getNotices(page, data.title, data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setNotices',response.data);
                            commit('setNoticesLoadStatus',4);
                        }
                        else{
                            commit('setNoticesLoadStatus',2);
                            commit('setLastPage',response.data.notices.meta.last_page);
                            commit('setCurrentPage',response.data.notices.meta.current_page);
                            commit('appendNotices',response.data.notices.data);
                        }

                    })
                    .catch(function () {
                        commit('setNoticesLoadStatus',3);
                    });
            }

        },
        loadNotice({commit,state},data){
            commit('setNoticeLoadStatus',1);

            if(data.accessToken!=='') {
                NoticeAPI.getNotice(data.slug,data.accessToken)
                    .then(function (response) {
                        if (response.data.response == false) {
                            commit('setNoticeLoadStatus', 4);
                        }
                        else
                            commit('setNotice', response.data.notice);
                            commit('setNoticeLoadStatus', 2);

                    })
                    .catch(function (response) {
                        commit('setNoticeLoadStatus',response);
                    })
            }
        },
        newNotice({commit},data){
            commit('setNoticeLoadStatus',1);
            if(data.accessToken!==''){
                NoticeAPI.postAddNewNotice(data.title,data.body,data.board_id,data.featured, data.image, data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setNoticeLoadStatus',4);
                            // commit('setNotices',response.data.);
                        }
                        else{
                            commit('setNoticeLoadStatus',2);
                            // commit('setNotices',response.data);
                        }
                    })
                    .catch(function () {
                        commit('setNoticeLoadStatus',3);
                    })
            }
        },
        updateNotice({commit,state},data){
            commit('setNoticeUpdateStatus',1);
            if(data.accessToken!==''){
                NoticeAPI.update(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setNoticeUpdateStatus',4);
                        }
                        else{
                            commit('setNoticeUpdateStatus',2);
                            // state.notices[data.index]=response.data.notice;
                        }
                    })
                    .catch(function () {
                        commit('setNoticeUpdateStatus',3);
                    })
            }
        },
        deleteNotice({commit},data){
            commit('setNoticeDeleteStatus',1);
            if(data.accessToken!==''){
                NoticeAPI.destroy(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setNoticeDeleteStatus',4);
                        }
                        else{
                            commit('setNoticeDeleteStatus',2);
                            commit('deleteNotice',data.index);
                        }
                    })
                    .catch(function () {
                        commit('setNoticeDeleteStatus',3);
                    })
            }
        },
        restoreNotice({commit},data){
            commit('setNoticeDeleteStatus',1);
            if(data.accessToken!==''){
                NoticeAPI.restore(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setNoticeDeleteStatus',4);
                        }
                        else{
                            commit('setNoticeDeleteStatus',5); //as an exception will use this status to define restore
                            commit('deleteTrashedNotice',data.index);
                        }
                    })
                    .catch(function () {
                        commit('setNoticeDeleteStatus',3);
                    })
            }
        },
        trashNotice({commit},data){
            commit('setNoticeDeleteStatus',1);
            if(data.accessToken!==''){
                NoticeAPI.trash(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setNoticeDeleteStatus',4);
                        }
                        else{
                            commit('setNoticeDeleteStatus',6);  //as an exception will use this status to define trash
                            commit('deleteTrashedNotice',data.index);
                        }
                    })
                    .catch(function () {
                        commit('setNoticeDeleteStatus',3);
                    })
            }
        },
        loadTrashedNotices({commit},data){
            commit('setTrashedNoticesLoadStatus',1);

            if(data.accessToken!==''){
                NoticeAPI.getTrashedNotices(1,data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setTrashedNoticesLoadStatus',4);
                        }
                        else{
                            commit('setTrashedNoticesLoadStatus',2);
                            commit('setTrashedNotices',response.data.notices.data);
                            commit('setLastPage',response.data.notices.meta.last_page);
                            commit('setCurrentPage',response.data.notices.meta.current_page);
                        }
                    })
                    .catch(function () {
                        commit('setTrashedNoticesLoadStatus',3);
                    });
            }
        },
        appendTrashedNotices({commit,state},data){
            commit('setTrashedNoticesLoadStatus',1);

            if(data.accessToken!=='' && state.currentPage<state.lastPage){
                let page=state.currentPage+1;
                NoticeAPI.getTrashedNotices(page,data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setTrashedNoticesLoadStatus',4);
                        }
                        else{
                            commit('setTrashedNoticesLoadStatus',2);
                            commit('appendTrashedNotices',response.data.notices.data);
                            commit('setLastPage',response.data.notices.meta.last_page);
                            commit('setCurrentPage',response.data.notices.meta.current_page);
                        }
                    })
                    .catch(function () {
                        commit('setTrashedNoticesLoadStatus',3);
                    });
            }
        },
    },
    mutations:{
        setBoard(state,board){
            state.board=board;
        },
        setNotices(state,notices){
            state.notices=notices;
        },
        appendNotices(state,notices){
            for(let x in notices){
                (state.notices).push(notices[x]);
            }
        },
        setNoticesLoadStatus(state,status){
            state.noticesLoadStatus=status;
        },
        setNotice(state,notice){
            state.notice=notice;
        },
        setNoticeLoadStatus(state,status){
            state.noticeLoadStatus=status;
        },
        setNoticeUpdateStatus(state,status){
            state.noticeUpdateStatus=status;
        },
        deleteNotice(state,index){
            (state.notices).splice(index,1);
        },
        setNoticeDeleteStatus(state,status){
            state.noticeDeleteStatus=status;
        },
        setTrashedNotices(state,notices){
            state.trashedNotices=notices;
        },
        appendTrashedNotices(state,notices){
            for(let x in notices){
                (state.trashedNotices).push(notices[x]);
            }
        },
        setTrashedNoticesLoadStatus(state,status){
            state.trashedNoticesLoadStatus=status;
        },
        deleteTrashedNotice(state,index){
            (state.trashedNotices).splice(index,1);
        },
        setLastPage(state,lastPage){
            state.lastPage=lastPage;
        },
        setCurrentPage(state,currentPage){
            state.currentPage=currentPage;
        }
    },
    getters:{
        getBoard(state){
            return state.board;
        },
        getNotices(state){
            return state.notices;
        },
        getNoticesLoadStatus(state){
            return state.noticesLoadStatus;
        },
        getNotice(state){
            return state.notice;
        },
        getNoticeLoadStatus(state){
            return state.noticeLoadStatus;
        },
        getNoticeUpdateStatus(state){
            return state.noticeUpdateStatus;
        },
        getNoticeDeleteStatus(state){
            return state.noticeDeleteStatus;
        },
        getTrashedNotices(state){
            return state.trashedNotices;
        },
        getTrashedNoticesLoadStatus(state){
            return state.trashedNoticesLoadStatus;
        },
        getLastPage(state){
            return state.lastPage;
        },
        getMorePages(state){
            return state.currentPage<state.lastPage
        }
    }
};