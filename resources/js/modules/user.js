import UserAPI from '../api/user.js';

export const user ={
    state: {
        user:{},
        userLoadStatus:0,
        userLoginStatus:0,
        message:'',
        // userToken:'',
        userUpdateStatus:'',
        isAdmin:false,
        isAuthor:false,
        Users:[],
        UsersLoadStatus:0,

    },
    actions:{
        updateUser({commit,state},data){
            commit('setUserUpdateStatus',1);

            UserAPI.postUpdateUser(data.code,data.year,data.account_number,state.user.access_token)
                .then(function (response) {
                    if(response.data.response==false){
                        //error occured, user should log in again
                        commit('setMessage',response.data.message);
                        commit('setUserUpdateStatus',4);
                    }
                    else{
                        commit('setUser',response.data.user);
                        commit('setUserUpdateStatus',2);
                    }

                })
                .catch(function () {
                    commit('setUserUpdateStatus',3);
                });


        },
        loadLogin({commit,state},data){
            commit('setUserLoginStatus',1);
            // commit('setUser',data);

            UserAPI.postLogin(data.google_id,data.name,data.email,data.avatar,data.google_token)
                .then(function (response) {

                    if(response.data.response==false){
                        commit('setUserLoginStatus',4);
                        commit('setMessage',response.data.message);
                    }
                    else{
                        //sets user
                        commit('setUser',response.data.user);
                        commit('setUserLoginStatus',2);

                        let roles=response.data.user.roles.data;

                        for (let role in roles){
                            if (roles[role].name==='Admin' || roles[role].name==='Developer'){
                                state.isAdmin=true;
                            }else if(roles[role].name==='Author')
                                state.isAuthor=true;
                        }
                    }

                })
                .catch(function (response) {
                    commit('setMessage',response.message);
                    commit('setUserLoginStatus',3);
                });


        },
        loadUsers({commit,state},data){
            commit('setUsersLoadStatus',1);
            // commit('incrementPage');

            if(data.accessToken!==''){
                UserAPI.getUsers(data.text,data.accessToken)
                    .then(function (response) {
                        if(response.data.response===false){
                            commit('setUsersLoadStatus',response.data.status);
                            commit('setUsers',[]);
                        }else{
                            commit('setUsersLoadStatus',2);

                            //remove current user from the list
                            let users=response.data.users;
                            for (let user in users){
                                if (users[user].google_id===state.user.google_id){
                                    users.splice(user,1);
                                }
                            }
                            commit('setUsers',users);
                        }
                        // commit('setCheck',response.data.candidates.meta.has_more_pages);
                        // commit('setCheck',response.data.candidates.meta.has_more_pages);

                    })
                    .catch(function (response) {
                        commit('setUsersLoadStatus',3);
                        // commit('setCheck',response.message);
                    });

            }
        },
        updateRole({commit,state},data){
            commit('setUserUpdateStatus',1);

            if(data.accessToken!==''){
                UserAPI.updateRole(data)
                    .then(function (response) {
                        if(response.data.response===false){
                            if(response.data.status===5)
                                commit('setUserUpdateStatus',5);
                            else
                                commit('setUserUpdateStatus',4);
                        }else{
                            commit('setUserUpdateStatus',2);
                        }

                    })
                    .catch(function (response) {
                        commit('setUserUpdateStatus',3);
                    });

            }
        },
        // signIn({commit,state},data){
        //     commit('setUserLoginStatus',1);
        //
        //     UserAPI.postSignUp(data)
        //         .then(function (response) {
        //             // alert(response.data);
        //             if(response.data.response==false){
        //                 commit('setUserLoginStatus',4);
        //             }
        //             else{
        //                 //sets user
        //                 commit('setUser',response.data.user);
        //                 commit('setUserLoginStatus',2);
        //                 // commit('setUser',response.data.user);
        //                 // commit('setUserLoginStatus',2);
        //             }
        //
        //         })
        //         .catch(function () {
        //             // commit('setUser',{});
        //             commit('setUserLoginStatus',3);
        //         });
        //
        //
        // },
        // loadUser({dispatch,commit,state},data){
        //     commit('setUserLoadStatus',1);
        //
        //     let google_id=data.google_id?data.google_id:state.user.google_id;
        //     if(google_id!==null){
        //         UserAPI.getUser(google_id)
        //             .then(function (response) {
        //                 if(response.data.response==false){
        //                     //invalid google id, user should log in again
        //                     commit('setUserLoadStatus',4);
        //                 }
        //                 else{
        //                     commit('setUser',response.data.user);
        //                     commit('setUserLoadStatus',2);
        //                 }
        //
        //             })
        //             .catch(function () {
        //                 commit('setUser',[]);
        //                 commit('setUserLoadStatus',3);
        //             });
        //     }
        //
        // },
        // loadToken({commit,state}){
        //     commit('setUserTokenLoadStatus',1);
        //     if(state.user.google_id!==''){
        //         UserAPI.getToken(state.user.google_id)
        //             .then(function (response) {
        //                 commit('setToken',response.data.user.access_token);
        //                 commit('setUserTokenLoadStatus',2);
        //             })
        //             .catch(function () {
        //                 commit('setToken','');
        //                 commit('setUserTokenLoadStatus',1);
        //             });
        //     }
        //
        // },
        logoutUser({commit}){
            commit('setUser',[]);
            commit('setMessage','');
            commit('setUserLoadStatus',0);
            commit('setUserLoginStatus',5);
        }
    },
    mutations:{
        setUsers(state,Users){
            state.Users=Users;
        },
        setUsersLoadStatus(state,status){
            state.UsersLoadStatus=status;
        },
        setUser(state,user){
            state.user=user;
        },
        setUserLoadStatus(state,status){
            state.userLoadStatus=status;
        },
        setMessage(state,message){
            state.message=message
        },
        setUserUpdateStatus(state,status){
            state.userUpdateStatus=status;
        },
        setUserLoginStatus(state,status){
            state.userLoginStatus=status;
        },
    },
    getters:{
        getUsers(state){
            return state.Users;
        },
        getUsersLoadStatus(state){
            return state.UsersLoadStatus;
        },
        getUser(state){
            return state.user;
        },
        getUserLoadStatus(state){
            return state.userLoadStatus;
        },
        getAccessToken(state){
            return state.user.access_token;
        },
        getGoogleId(state){
            return state.user.google_id;
        },
        getUserUpdateStatus(state){
            return state.userUpdateStatus;
        },
        getUserLoginStatus(state){
            return state.userLoginStatus;
        },
        getAdmin(state){
            return state.isAdmin;
        },
        getAuthor(state){
            return state.isAuthor;
        },


    }
};