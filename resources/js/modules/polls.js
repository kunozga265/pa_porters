/*
|-------------------------------------------------------------------------------
| VUEX modules/polls.js
|-------------------------------------------------------------------------------
| The Vuex data store for the polls
*/

import PollAPI from '../api/Poll.js';

export const polls = {
    state: {
        polls:[],
        pollsLoadStatus:0,
        pollsLastPage:0,
        pollsCurrentPage:1,
        poll:[],
        pollLoadStatus:0,
        voteLoadStatus:0,
        pollDeleteStatus:0,

    },
    actions:{
        loadPolls({commit},data){
            commit('setPollsLoadStatus',1);

            if(data.accessToken!==''){
                PollAPI.getPolls(data.page,data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setPolls',response.data);
                            commit('setPollsLoadStatus',4);
                        }
                        else{
                            commit('setPollsLoadStatus',2);
                            commit('setPolls',response.data.polls.data);
                            commit('setPollsLastPage',response.data.polls.meta.last_page);
                            commit('setPollsCurrentPage',response.data.polls.meta.current_page);
                        }

                    })
                    .catch(function (response) {
                        commit('setPolls',response.message);
                        commit('setPollsLoadStatus',3);
                    });
            }

        },
        appendPolls({commit,state},data){
            commit('setPollsLoadStatus',1);

            if(data.accessToken!=='' && state.pollsCurrentPage<state.pollsLastPage){
                let page=state.pollsCurrentPage+1;
                PollAPI.getPolls(page,data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setPolls',response.data);
                            commit('setPollsLoadStatus',4);
                        }
                        else{
                            commit('setPollsLoadStatus',2);
                            commit('appendPolls',response.data.polls.data);
                            commit('setPollsLastPage',response.data.polls.meta.last_page);
                            commit('setPollsCurrentPage',response.data.polls.meta.current_page);
                        }

                    })
                    .catch(function (response) {
                        commit('setPollsLoadStatus',3);
                    });
            }

        },
        loadPoll({commit},data){
            commit('setPollLoadStatus',1);

            if(data.accessToken!==''){
                PollAPI.getPoll(data.id,data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setPoll',response.data);
                            commit('setPollLoadStatus',4);
                        }
                        else{
                            commit('setPollLoadStatus',2);
                            commit('setPoll',response.data.poll);
                        }

                    })
                    .catch(function (response) {
                        commit('setPoll',response.message);
                        commit('setPollLoadStatus',3);
                    });
            }

        },
        newPoll({commit},data){
            commit('setPollLoadStatus',1);
            if(data.accessToken!==''){
                PollAPI.postAddNewPoll(data.title,data.description,data.options,data.start_date,data.end_date,data.type, data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setPollLoadStatus',4);

                        }
                        else{
                            commit('setPollLoadStatus',2);
                            // commit('setPolls',response.data);
                        }
                    })
                    .catch(function () {
                        commit('setPollLoadStatus',3);
                    })
            }
        },
        storeVote({commit,state},data){
            commit('setVoteLoadStatus',1);
            if(data.accessToken!==''){
                PollAPI.postVote(data.option_id, data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setVoteLoadStatus',4);
                        }
                        else{
                            commit('setVoteLoadStatus',2);
                            state.polls[data.index]=response.data.poll
                        }
                    })
                    .catch(function () {
                        commit('setVoteLoadStatus',3);
                    })
            }
        },
        deletePoll({commit,state},data){
            commit('setPollDeleteStatus',1);
            if(data.accessToken!==''){
                PollAPI.deletePoll(data.id, data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setPollDeleteStatus',4);
                            commit('setPollsLastPage',response.data);
                        }
                        else{
                            commit('setPollDeleteStatus',2);
                            (state.polls).splice(data.index,1)
                        }
                    })
                    .catch(function (response) {
                        commit('setPollDeleteStatus',3);
                        commit('setPollsLastPage',response.message);
                    })
            }
        }
    },
    mutations:{
        setPolls(state,Polls){
            state.polls=Polls;
        },
        appendPolls(state,polls){
            for(let x in polls){
                (state.polls).push(polls[x]);
            }
        },
        setPollsLoadStatus(state,status){
            state.pollsLoadStatus=status;
        },
        setPoll(state,Poll){
            state.poll=Poll;
        },
        setPollLoadStatus(state,status){
            state.pollLoadStatus=status;
        },
        setPollsLastPage(state,lastPage){
            state.pollsLastPage=lastPage;
        },
        setPollsCurrentPage(state,currentPage){
            state.pollsCurrentPage=currentPage;
        },
        setVoteLoadStatus(state,status){
            state.voteLoadStatus=status;
        },
        setPollDeleteStatus(state,status){
            state.pollDeleteStatus=status;
        },
    },
    getters:{
        getPolls(state){
            return state.polls;
        },
        getPollsLoadStatus(state){
            return state.pollsLoadStatus;
        },
        getPoll(state){
            return state.poll;
        },
        getPollLoadStatus(state){
            return state.pollLoadStatus;
        },
        getPollsLastPage(state){
            return state.pollsLastPage;
        },
        getVoteLoadStatus(state){
            return state.voteLoadStatus;
        },
        getPollDeleteStatus(state){
            return state.pollDeleteStatus;
        },
        getMorePollsPages(state){
            return state.pollsCurrentPage<state.pollsLastPage
        }

    }

};