/*
|-------------------------------------------------------------------------------
| VUEX modules/programs.js
|-------------------------------------------------------------------------------
| The Vuex data store for the programs
*/

import ProgramAPI from '../api/program.js';

export const programs = {
    state: {
        programs:[],
        programsLoadStatus:0,

        program:{},
        programLoadStatus:0,
    },
    actions:{
        loadPrograms({commit},data){
            commit('setProgramsLoadStatus',1);

            if(data.accessToken!==''){
                ProgramAPI.getPrograms(data.accessToken)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setPrograms',response.data);
                            commit('setProgramsLoadStatus',4);
                        }
                        else{
                            commit('setPrograms',response.data.programs.data);
                            commit('setProgramsLoadStatus',2);
                        }

                    })
                    .catch(function (response) {
                        // commit('setPrograms',response.message);
                        commit('setProgramsLoadStatus',3);
                    });
            }

        },

        clearPrograms({commit}){
            commit('setPrograms',[]);
            commit('setProgramsLoadStatus',0);
        }
    },
    mutations:{
        setPrograms(state,notices){
            state.programs=notices;
        },
        setProgramsLoadStatus(state,status){
            state.programsLoadStatus=status;
        },
        // setNotice(state,notice){
        //     state.notice=notice;
        // },
        // setNoticeLoadStatus(state,status){
        //     state.noticeLoadStatus=status;
        // }
    },
    getters:{
        getPrograms(state){
            return state.programs;
        },
        getProgramsLoadStatus(state){
            return state.programsLoadStatus;
        },
        // getNotice(state){
        //     return state.notice;
        // },
        // getNoticeLoadStatus(state){
        //     return state.noticeLoadStatus;
        // }

    }

};