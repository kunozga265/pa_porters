window._ = require('lodash');
window.Popper = require('popper.js').default;


try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


import Vue from 'vue';
import router from './routes';
import store from './store';

//importing vuetify
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify,{
    theme:{
        primary: '#1d55cc',
        secondary: '#6aebff',
        tetiary: '#555555',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
}
});

//importing ckeditor
import CKEditor from '@ckeditor/ckeditor5-vue'
Vue.use(CKEditor);


router.beforeEach((to,from,next)=>{
    if (to.matched.some(record=>record.meta.guest)){
        if(store.getters.getUserLoginStatus===2)
            from();
        else
            next()
    }else if (to.matched.some(record=>record.meta.auth)){
        if(store.getters.getUserLoginStatus!==2)
            next('/sign-in');
        else{
            if (to.matched.some(record=>record.meta.admin)) {
                    if (store.getters.getAdmin !== true){
                        if (to.matched.some(record=>record.meta.author)) {
                            if (store.getters.getAuthor !== true)
                                from();
                            else
                                next()
                        }
                    }
                    else
                        next();
                }
            else
                next()
        }
    }else {
        next()
    }
});

new Vue({
    router,
    store
}).$mount('#app');


// import Vue

