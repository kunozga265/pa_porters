<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pa Porters</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-signin-scope" content="profile email">
    {{--<meta name="google-signin-client_id" content="499781986537-j45asi8r3k3msg39t382i9bon1nlo7h0.apps.googleusercontent.com">--}}

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">
    <link href="{{asset('/css/material-design-icons/iconfont/material-icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('/css/app.css')}}" rel="stylesheet" type="text/css"/>

    <link rel="icon" type="image/x-icon" href="{{asset('/favicon.png')}}">

    {{--<script src="https://apis.google.com/js/platform.js" async defer></script>--}}
    <script src="https://apis.google.com/js/api:client.js"></script>


    <script type='text/javascript'>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div>
        {{--<p>Logged in</p>--}}
        {{--<a href="/logout">Logout</a>--}}
    </div>
    <div id="app">
        <router-view></router-view>
    </div>

    <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
</body>
</html>