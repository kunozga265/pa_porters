const mix = require('laravel-mix');
// const webpack = require('webpack');
const VueLoaderPlugin=require('vue-loader/lib/plugin');
// const {VueLoaderPlugin}=require('vue-loader');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

module.exports={
    mode:'development',
    module: {
        rules: [
            {
                test:/\.vue$/,
                loader: 'vue-loader'
            },
            {
                test:/\.vue\.html$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: Config.babel()
                    }
                ]
            },{
                test: /\css$/,
                use:[
                    'vue-style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    plugins:[
        new VueLoaderPlugin()
    ]
}



mix.js('resources/js/app.js', 'public/js')
    .webpackConfig({
        resolve: {
            alias: {
                '@': path.resolve('resources/assets/sass')
            }
        }
    })
   .sass('resources/sass/app.scss', 'public/css').version().sourceMaps();
