<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }

    protected $fillable=[
        'name',
        'code',
        'year'
    ];

    protected $hidden=[
      'created_at',
      'updated_at',
    ];
}
