<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public function poll()
    {
        return $this->belongsTo('App\Poll');
    }

//    public function users()
//    {
//        return $this->hasMany('App\User','google_id','google_id');
//    }

    protected $fillable=[
        'option_id',
        'google_id',
        'poll_id',
    ];

    protected $hidden=[
        'updated_at',
        'created_at',
    ];
}
