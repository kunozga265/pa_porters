<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Notice;
use Illuminate\Http\Request;
use App\Http\Requests\BoardRequest;
use App\Board;
use App\Http\Resources;

class BoardController extends Controller
{
    /*
    |-------------------------------------------------------------------------------
    | Gets all the boards
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/boards
    | Controller:     API\BoardController
    | Method:         GET
    | Description:
    */
    public function getBoards()
    {
        $boards=Board::all();
        if (is_object($boards)){
            return response(['response'=>true,'boards'=>new Resources\BoardCollection($boards)],200);
        }

        return response(['response'=>false,'message'=>'No boards'],400);
    }
    /*
    |-------------------------------------------------------------------------------
    | Gets a board
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/boards/:title
    | Controller:     API\NoticeController
    | Method:         GET
    | Description:
    */
    public function getBoard($title)
    {
        $board=Board::where('title',$title)->first();

        if (is_object($board)){
            $response=[
                'board'=>$board,
                'notices'=>new Resources\NoticeCollection($board->notices()->latest()->paginate(6))
            ];
            return response()->json($response,200);
        }
        return response()->json(false,404);
    }
    /*
    |-------------------------------------------------------------------------------
    | Adds board to the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/boards/new
    | Controller:     API\BoardController
    | Method:         POST
    | Description:
    */
    public function postNewBoard(BoardRequest $request){
        $board=new Board([
            'title'=>strtolower($request->title),
            'image'=>'',
        ]);
        $board->save();

        return new Resources\BoardResource($board);
    }
    /*
    |-------------------------------------------------------------------------------
    | Updates a board
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/boards/{title}
    | Controller:     API\BoardController
    | Method:         POST
    | Description:    Returns the updated board
    */
    public function postUpdateBoard(BoardRequest $request,$title)
    {
        $board=Board::where('title',$title);

        if (is_object($board)){

            $board->update([
                'title'=>strtolower($request->title),
               'image'  =>'',
            ]);

            return new Resources\BoardResource($board);
        }
        return response()->json(false,404);

    }
    /*
    |-------------------------------------------------------------------------------
    | Deletes a board in the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/board/{slug}
    | Controller:     API\NoticeController
    | Method:         DELETE
    | Description:    Returns a success response or a failed response
    */
    public function deleteBoard($title)
    {
        $board=Board::where('title',$title);
        if (is_object($board)){
            $board->delete();
            return response()->json(true,200);
        }
        return response()->json(false,404);

    }

}
