<?php

namespace App\Http\Controllers\API;

use App\Notice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Poll;
use App\Vote;
use App\Http\Resources;

use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use Carbon\Carbon;

class AppController extends Controller
{
    public function getApp()
    {
        return view('app');
    }

    /*
    |-------------------------------------------------------------------------------
    | Gets all the notices
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/notices/get/{timestamp}
    | Controller:     API\NoticeController
    | Method:         GET
    | Middleware      JWTAuth (requires token), Roles
    | Allowed Roles:  *
    | Description:    Returns notices posted after the timestamp
    | Parameters:
    |   timestamp-> last time a user synced data
    */
    public function getAppUpdates(Request $request, $timestamp)
    {
        //$time=(date('Y-m-d H:i:s',$timestamp));
        $time=Carbon::createFromTimestamp($timestamp);
        $now=Carbon::now();
        $diff=$time->diff($now);

        //if its less than a year and greater than a month then subtract one month from the current date
        if($diff->y > 0){
            $current_time= $now->subMonth();
        }
        else{
            //if it's greater than a month then subtract one month from the current date
            if($diff->m > 0)
                $current_time= $now->subMonth();
            else
                $current_time=$time;
        }

        $current_timestamp=$current_time->getTimestamp();
        $current_time=$current_time->toDateTimeString();


        //get user
        $user=UserController::getAdmin($request);

        //getting the active polls
        $polls=Poll::where('end_date', '>', "$current_timestamp")->latest()->get();

        $notices=null;

        //deleted notices
        $deleted_notices=Notice::onlyTrashed()->where('deleted_at', '>', "$current_time")->get();

        //check to see if they have subscribed to a board
        if ($user->boards->count()>0){
            $boards=[];
            foreach ($user->boards as $board){
                array_push($boards,$board->id);
            }

            $notices=Notice::whereIn('board_id',$boards)->where('updated_at', '>', "$current_time")->latest()->get();

        }

        //if both the notices and the polls are 0
        if ($notices->count()==0 &&$polls->count()==0){
            return response()->json([
                'response'          =>  false,
            ]);
        }else{
            return response()->json([
                'response'          =>  true,
                'notices'           =>  Resources\NoticeAppResource::collection($notices),
                'deleted_notices'   =>  $deleted_notices,
                'polls'             =>  Resources\PollResource::collection($polls),
            ]);
        }

    }

    public function postFeedback(Request $request){
        $this->validate($request,[
            'message'   =>  'required|min:5',
//            'email'     =>  'required',
//            'name'      =>  'required',
        ]);

        $data=[
            'subject'       =>  'Poly Noticeboard App Feedback',
            'email'         =>  'dummymail@example.com',
//            'email'         =>  $request->email,
//            'name'          =>  $request->name,
            'bodyMessage'   =>  $request->message
        ];


        // Create the Transport
        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465,'ssl'))
            ->setUsername('beee14-kmlowoka@poly.ac.mw')
            ->setPassword('kunopoly')
        ;

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message = (new Swift_Message($data['subject']))
            ->setFrom($data['email'])
//            ->setTo(['pnt-17bdec@inbox.mailtrap.io'])
            ->setTo(['polynoticeboard@gmail.com'])
            ->setBody("<strong>You have a new Message from the Poly Noticeboard App</strong><br><br><div>$data[bodyMessage]</div>",'text/html')
        ;

        // Send the message
        try{
        $result = $mailer->send($message);
            //if successful
            if($result==1)
                return response()->json(['response' => true]);
            else
                return response()->json(['response' => false]);
        }catch (\Swift_TransportException $exception){
            return response()->json(['response' => false,'status'=>$exception]);
        }
    }
}
