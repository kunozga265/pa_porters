<?php

namespace App\Http\Controllers\API;

use App\Poll;
use App\Option;
use App\Vote;

use App\Http\Resources;
use App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\UserController;

class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $polls=Poll::orderBy('updated_at','DSC')->paginate(6);
//        $polls=Poll::paginate(2);
        return response()->json([
//            'polls'    =>  new Resources\PollCollection($polls)
            'polls'    => new Resources\PollCollection($polls)
        ]);
    }

    public function getPoll($id)
    {
        $poll=Poll::findOrFail($id);
        if (is_object($poll)){
            $response=[
                'response'  =>  true,
                'poll'    =>  new Resources\PollResource($poll)
            ];
            return response()->json($response,200);
        }
        return response()->json(['response'=>false],404);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PollRequest $request)
    {
        $user=UserController::getAdmin($request);

        //create poll
        $poll=new Poll([
            'title'         =>  $request->title,
            'author_id'     =>  $user->google_id,
            'description'   =>  $request->description,
            'type'          =>  $request->type,
            'start_date'    =>  $request->start_date,
            'end_date'      =>  $request->end_date,
        ]);
        $poll->save();

        //create options
        foreach ($request->options as $option){
            $option=new Option([
                'option'    =>  isset($option['google_id'])?$option['google_id']:$option,
                'poll_id'   =>  $poll->id
            ]);
            $option->save();
        }
        return response()->json([
            'poll'    =>  new Resources\PollResource($poll)
        ]);

    }

    public function postVote(Requests\VoteRequest $request)
    {
        $user=UserController::getAdmin($request);
        $option=Option::find($request->option_id);

        if (is_object($option)){
            $poll=$option->poll;
            $vote=$poll->votes()->where('google_id',$user->google_id)->first();

            if(!is_object($vote) && is_object($poll)){
                $vote=new Vote([
                    'option_id' =>  $option->id,
                    'poll_id'   =>  $poll->id,
                    'google_id' =>  $user->google_id
                ]);
                $vote->save();

                return response()->json([
                    'response'  =>  true,
                    'poll'      =>  new Resources\PollResource($vote->poll)
                ]);
            }
            return response()->json([
                'response'  =>  false,
                'status'    =>  'Voted already'
            ]);
        }


        return response()->json([
            'response'  =>  false,
            'status'    =>  'Option doesn\'t exist'
        ]);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    //COMPLICATES THINGS
//    public function update(Requests\PollRequest $request, $id)
//    {
//        $poll=Poll::find($id);
//        $poll->update([
//            'title'         =>  $request->title,
//            'description'   =>  $request->description,
//        ]);
//
//        foreach ($poll->votes() as $vote){
//            $vote->delete();
//        }
//
//        //create options
//        foreach ($request->options as $option){
//            $option=new Option([
//                'option'    =>  $option,
//                'poll_id'   =>  $poll->id
//            ]);
//            $option->save();
//        }
//        return response()->json([
//            'poll'    =>  new Resources\PollResource($poll)
//        ]);
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function deletePoll($id)
    {
        $poll=Poll::findOrFail($id);
        if ($poll->votes){
            foreach ($poll->votes as $vote){
                $vote->delete();
            }
        }
        if ($poll->options){
            foreach ($poll->options as $option){
                $option->delete();
            }
        }

        $poll->delete();
        return response()->json([
            'response'    =>  true
        ]);
    }
}
