<?php

namespace App\Http\Controllers\API;
use App\Board;
use App\Notice;
use App\Http\Resources;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\NoticeRequest;

use App\Http\Controllers\API\UserController;

use Illuminate\Support\Facades\Storage;
use Aws\S3\Exception\S3Exception;

use GuzzleHttp\Client;

use Intervention\Image\Facades\Image;
use Mews\Purifier\Facades\Purifier;
use Illuminate\Support\Facades\File;



class NoticeController extends Controller
{
    /*
    |-------------------------------------------------------------------------------
    | Gets all the notices
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/notices
    | Controller:     API\NoticeController
    | Method:         GET
    | Description:
    */
    public function getNotices()
    {
        $notices = Notice::all();
        return new Resources\NoticeCollection($notices);
    }

    /*
    |-------------------------------------------------------------------------------
    | Gets a notice
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/notices/:id
    | Controller:     API\NoticeController
    | Method:         GET
    | Description:
    */
    public function getNotice($slug)
    {
        $notice=Notice::where('slug',$slug)->first();
        if (is_object($notice)){
            $response=[
                'response'  =>  true,
                'notice'    =>  new Resources\NoticeResource($notice)
            ];
            return response()->json($response,200);
        }
        return response()->json(['response'=>false],404);

    }

    /*
   |-------------------------------------------------------------------------------
   | Adds notice to the database
   |-------------------------------------------------------------------------------
   | URL:            /api/v1/notices/new
   | Controller:     API\NoticeController
   | Method:         POST
   | Description:
   */
    public function postNewNotice(NoticeRequest $request)
    {
        $notice=new Notice();

        $slug=str_slug($request->title).'-'.date('Y-m-d-H-i-s');
        $notice->title=$request->title;
        $notice->body=Purifier::clean($request->body);
        $notice->slug=$slug;
        $notice->board_id=$request->board_id;
        $notice->featured=$request->featured;
//        $notice->image='https://cdn.vuetifyjs.com/images/cards/sunshine.jpg';
//        $notice->image_thumb='https://cdn.vuetifyjs.com/images/cards/sunshine.jpg';

        $user=UserController::getAdmin($request);

        $notice->user_id=$user->id;

        if (isset($request->image)){
            $explodedImage=explode(',',$request->image);
            $decodedImage=base64_decode($explodedImage[1]);

            $ext=$this->getExtension($explodedImage);

            $image_name="images/notices/".$slug.".".$ext;
            $image_name_thumb="images/notices/thumbnails/".$slug.".".$ext;

            try{
                //Making the image and resizing
                $image_thumb=Image::make($decodedImage)->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image=Image::make($decodedImage);

                //preparing to store in database
//                $resource_thumb = $image_thumb->stream()->detach();
//
//                //saving to amazing s3 storage
//                Storage::disk('s3')->put($image_name_thumb, $resource_thumb);
//                Storage::disk('s3')->put($image_name,$decodedImage);
                //saving to local storage

                $image_thumb->save($image_name_thumb);
                $image->save($image_name);


            }catch (/*S3Exception $exception*/\RuntimeException $e){
                return response()->json([
                    'response'=>false,
                    'status'=>'Could not upload picture',
                ]);
            }

            $notice->image= $image_name;
            $notice->image_thumb = $image_name_thumb;

        }
        else{
            //setting a default board image
            $number=rand(1,6);
            $board=Board::find($request->board_id);
            $notice->image="images/boards/$board->title-$number.jpg";
            $notice->image_thumb="images/boards/$board->title-$number.jpg";
        }


        $notice->save();

        //notification
        try{
            $client=new Client();
            $board_title=$notice->board->title=='PSU'||$notice->board->title=='administration'?'all':$notice->board->title;
            $notificationRequest=$client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers'=>[
                    'Authorization' => 'key=AIzaSyAKD3vZ9vs0zUvMumjjJrpLV0UA8H_4ljU',
                    'Content-Type'   =>  'application/json',
                ],
                'json'=>[
                    "to"=>"/topics/$board_title",
                    "data"=> [
                        'title' =>  $notice->title,
                        'body' =>  $notice->body,
                    ]
                ]
            ]);

            if ($notificationRequest->getStatusCode()==200){
                $notification=true;
            }else{
                $notification=false;
            }

        }catch (\GuzzleHttp\Exception\GuzzleException $e){
            $notification=false;
        }

        return response()->json([
            'response'      =>  true,
            'notification'  =>  $notification
            ]);

    }

    /*
    |-------------------------------------------------------------------------------
    | Updates a notice
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/notices/update/{slug}
    | Controller:     API\NoticeController
    | Method:         POST
    | Description:    Returns the updated notice
    */

    public function postUpdateNotice(NoticeRequest $request,$slug)
    {

        $notice=Notice::where('slug','=',$slug)->first();

        $slug=str_slug($request->title).'-'.date('Y-m-d-H-i-s');

        if(is_object($notice)){

            if (isset($request->image)){
                if ($notice->image){
//                    try{
//                        Storage::disk('s3')->delete($notice->image);
//                    }catch (S3Exception $exception){
//                        return response()->json(['response'=>false]);
//                    }
                    //Delete old picture
                    File::delete($notice->image);
                    File::delete($notice->image_thumb);
                }

                $explodedImage=explode(',',$request->image);
                $decodedImage=base64_decode($explodedImage[1]);

                $ext=$this->getExtension($explodedImage);

                $image_name="images/notices/".$slug.".".$ext;
                $image_name_thumb="images/notices/thumbnails/".$slug.".".$ext;

                try{
                    //Making the image and resizing
                    $image_thumb=Image::make($decodedImage)->resize(null, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $image=Image::make($decodedImage);

                    //saving to local storage
                    $image_thumb->save($image_name_thumb);
                    $image->save($image_name);


                }catch (/*S3Exception $exception*/\RuntimeException $e){
                    return response()->json([
                        'response'=>false,
                        'status'=>'Could not upload picture',
                    ]);
                }
                $notice->update([
                    'image'              =>  $image_name,
                    'image_thumb'        =>  $image_name,
                ]);
            }

            $notice->update([
                'title'      =>  $request->title,
                'body'       =>  Purifier::clean($request->body),
                'slug'       =>  $slug,
                'board_id'  =>  $request->board_id,
                'featured'  =>  $request->featured
            ]);

//            return new Resources\NoticeResource($notice);
            return response()->json([
                'response'      =>  true,
//                'notice'        =>  new Resources\NoticeResource($notice)
            ]);
        }

        return response()->json(['response' =>  true,],404);
    }

    public function getExtension($explodedImage)
    {
        $imageExtensionDecode=explode('/',$explodedImage[0]);
        $imageExtension=explode(';',$imageExtensionDecode[1]);
        if($imageExtension[0]=='jpeg')
            return 'jpg';
        else
            return 'png';
    }

    public function postUpload( Request $request)
    {

        if (isset($request->image)){
            $explodedImage=explode(',',$request->image);
            $decodedImage=base64_decode($explodedImage[1]);

            $ext=$this->getExtension($explodedImage);
            //$filename=

            try{
                //dd(Storage::disk('s3')->put('images',$decodedImage));
                $image_name=Storage::disk('s3')->put('images.jpg',$decodedImage);

            }catch (S3Exception $exception){
//                dd($exception->getMessage());
                return response()->json([
                    'response'=>false,
                    'status'=>'Could not upload picture',
                ]);
            }

            return response()->json([
                'response'=>true,
                'status'=>$image_name,
            ]);

        }else{
            return response()->json([
                'response'=>false,
                'status'=>'No Image',
            ]);
        }
    }

    /*
    |-------------------------------------------------------------------------------
    | Deletes a notice in the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/notices/{slug}
    | Controller:     API\NoticeController
    | Method:         DELETE
    | Description:    Returns a success response or a failed response
    */

    public function deleteNotice($slug)
    {
        $notice=Notice::where('slug','=',$slug)->first();

        if(is_object($notice)){
            $notice->delete();
            return response()->json([
                'response'  =>true,
                'message'   =>"Successfully deleted."
            ],200);
        }
        return response()->json([
            'response'  =>false,
            'message'   =>"Unsuccessfully deleted."
        ],400);
    }

    /*
    |-------------------------------------------------------------------------------
    | Displays only soft deleted notices
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/notices/trashed
    | Controller:     API\NoticeController
    | Method:         DELETE
    | Middleware      JWTAuth (requires token), Roles
    | Allowed Roles:  Developer, Admin
    | Description:    Returns a true if successful false otherwise
    */
    public function getTrashedNotices()
    {
        $notices=Notice::onlyTrashed()->latest()->paginate(6);
//        if ($notices->count()==0){
//            return response()->json([
//                'response1' => false
//            ]);
//
//        }

        return response()->json([
            'response'          => true,
            'notices'           => new Resources\NoticeCollection($notices)
        ]);

    }

    /*
   |-------------------------------------------------------------------------------
   | Permanently deletes a notice in the database
   |-------------------------------------------------------------------------------
   | URL:            /api/v1/notices/{slug}/trashed
   | Controller:     API\NoticeController
   | Method:         DELETE
   | Middleware      JWTAuth (requires token), Roles
   | Allowed Roles:  Developer, Admin
   | Description:    Returns a true if successful false otherwise
   | Parameters:
   |   slug -> slug of the notice
   */

    public function deleteTrashNotice($slug)
    {
        $notice=Notice::onlyTrashed()->where('slug','=',$slug)->first();

        if(is_object($notice)){
            if($notice->image){
                $explodedImage=explode('/',$notice->image);
                if($explodedImage[1]!=='boards'){
                    File::delete($notice->image);
                    File::delete($notice->image_thumb);
                }
            }
            $notice->forceDelete();
            return response()->json([
                'response'  =>true,
                'message'   =>"Successfully deleted."
            ],200);
        }
        return response()->json([
            'response'  =>false,
            'message'   =>"Unsuccessfully deleted."
        ],400);
    }


    /*
    |-------------------------------------------------------------------------------
    | Restores a soft deleted notice in the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/notices/{slug}/restore
    | Controller:     API\NoticeController
    | Method:         DELETE
    | Middleware      JWTAuth (requires token), Roles
    | Allowed Roles:  Developer, Admin
    | Description:    Returns a true if successful false otherwise
    | Parameters:
    |   slug -> slug of the notice
    */
    public function getRestoreNotice($slug)
    {
        $notice=Notice::onlyTrashed()->where('slug','=',$slug)->first();

        if(is_object($notice)){
            $notice->restore();
            return response()->json([
                'response'  =>true,
                'message'   =>"Successfully restored."
            ],200);
        }
        return response()->json([
            'response'  =>false,
            'message'   =>"Unsuccessfully restored."
        ]);

    }



}
