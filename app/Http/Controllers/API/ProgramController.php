<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;
use App\Http\Resources;
use App\Http\Requests\ProgramRequest;

class ProgramController extends Controller
{
    /*
    |-------------------------------------------------------------------------------
    | Gets all the programs
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/programs
    | Controller:     API\ProgramController
    | Method:         GET
    | Description:
    */
    public function getPrograms()
    {
        $programs=Program::all();

        return response()->json([
            'response'  => true,
            'programs'   => new Resources\ProgramCollection($programs)
        ],200);
    }
    /*
    |-------------------------------------------------------------------------------
    | Gets a program
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/programs/:title
    | Controller:     API\ProgramController
    | Method:         GET
    | Description:
    */
    public function getProgram($code)
    {
        $program=Program::where('code',$code)->first();

        if (is_object($program)){
            $response=[
                'response'  => true,
                'program'   =>  new Resources\ProgramResource($program),
                'students'  =>  $program->users
            ];
            return response()->json(compact('response'),200);
        }
        return response()->json(false,404);
    }

    /*
    |-------------------------------------------------------------------------------
    | Adds program to the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/programs/new
    | Controller:     API\ProgramController
    | Method:         POST
    | Description:
    */
    public function postNewProgram(ProgramRequest $request){
        $program=new Program([
            'name'=>strtolower($request->name),
            'code'=>strtolower($request->code),
            'year'=>$request->year
        ]);
        $program->save();

        return new Resources\ProgramResource($program);
    }

    /*
    |-------------------------------------------------------------------------------
    | Updates a program
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/programs/{code}
    | Controller:     API\ProgramController
    | Method:         POST
    | Description:    Returns the updated board
    */
    public function postUpdateProgram(ProgramRequest $request,$code)
    {
        $program=Program::where('code',$code)->first();

        if (is_object($program)){

            $program->update([
                'name'=>strtolower($request->name),
                'code'=>strtolower($request->code),
                'year'=>$request->year,
            ]);

            return new Resources\ProgramResource($program);
        }
        return response()->json(false,404);

    }
    /*
    |-------------------------------------------------------------------------------
    | Deletes a program in the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/programs/{code}
    | Controller:     API\ProgramController
    | Method:         DELETE
    | Description:    Returns a success response or a failed response
    */
    public function deleteProgram($code)
    {
        $program=Program::where('code',$code);
        if (is_object($program)){
            $program->delete();
            return response()->json(true,200);
        }
        return response()->json(false,404);

    }
}
