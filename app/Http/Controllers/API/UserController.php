<?php

namespace App\Http\Controllers\API;
use App\Board;
use App\Http\Controllers\Controller;

//models
use App\User;
use App\Role;
use App\Program;

//requests

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;

//resources
use App\Http\Resources;

//jwt-auth
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class UserController extends Controller
{
    const AWSDOMAINNAME='https://s3.amazonaws.com/pa-porters/';

    public function search(Request $request)
    {
        $users=User::where('name','LIKE',"%$request->text%")->get();

        if ($users->count()>20){
            return response()->json([
                'response'  =>  false,
                'status'    =>  5
            ]);
        }else if ($users->count()==0){
            return response()->json([
                'response'  =>  false,
                'status'    =>  6
            ]);
        }

        return response()->json([
            'response'  =>  true,
            'users' => Resources\UserResource::collection($users),
        ]);

    }

    /*
    |-------------------------------------------------------------------------------
    | Gets the authenticated user
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Method:         GET
    | Controller:     API\UserController
    | Middleware:     JWTAuth (requires token)
    | Description:    Returns a a user object
    */
//    public function getAuthenticatedUser()
//    {
//        try {
//
//            if (! $user = JWTAuth::parseToken()->authenticate()) {
//                return response()->json(['user_not_found'], 404);
//            }
//
//        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
//
//            return response()->json(['token_expired'], $e->getStatusCode());
//
//        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
//
//            return response()->json(['token_invalid'], $e->getStatusCode());
//
//        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
//
//            return response()->json(['token_absent'], $e->getStatusCode());
//
//        }
//
//        $user=new Resources\UserResource($user);
//
//        return response()->json(compact('user'));
//    }
    /*
    |-------------------------------------------------------------------------------
    | Checks whether or not a user exists in the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/authenticate/{google_id}
    | Method:         GET
    | Description:    Returns a true if user exists and false if not
    | Parameters:
    |   $google_id   -> ID of the user we are authenticating
    */
    public function getAuthenticateUser($google_id)
    {
        //gets the user from the database
        $user = User::where( 'google_id', '=', $google_id)->first();

        if (is_object($user)){
//            $token = JWTAuth::fromUser($user);
//
//            $user->update([
//                'access_token'=>$token
//            ]);

//            $user= new Resources\UserResource($user);
//            $response = true;
//            return response()->json(compact('response','user'),201);
            return response()->json(['response'=>true]);

        }
        return response()->json(['response'=>false]);
    }
    /*
    |-------------------------------------------------------------------------------
    | Gets a new token for a user
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/newToken/{google_id}
    | Controller:     API\UserController
    | Method:         GET
    | Description:    Returns the user with the new token
    */
    public function getNewToken($google_id)
    {
        $user = User::where( 'google_id', '=', $google_id)->first();

        if (is_object($user)){
            $credentials = [
                'email'     => $user->email,
                'password'  => $user->google_id
            ];

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['response'=>false, 'message' => 'Invalid Credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['response'=>false, 'message' => 'Could not create token'], 500);
            }

            $user->update([
                'access_token'=>$token
            ]);

            $data=[
                'response'  => true,
                'access_token'=>$token
            ];
            return response()->json($data,201);
        }else
            return response()->json(['response'=>false, 'message'=>'User doesn\'t exist'],400);

    }

    /*
    |-------------------------------------------------------------------------------
    | Adds user to the database
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/add-to-database
    | Controller:     API\UserController
    | Method:         POST
    | Description:    Adds user to the database
    */
    public function postLogin (Request $request)
    {
        $validator=Validator::make($request->all(),[
            'google_id'     => 'required',
            'name'          => 'required',
            'email'         => 'required',
            'avatar'        => 'required',
            'google_token'  => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>'Missing Fields'],400);
        }

        //checks if the user has already been added into the database
        $user = User::where( 'google_id', '=', $request->google_id)->first();

        if (is_object($user)){
            //create new token
            $token = JWTAuth::fromUser($user);

            if ($request->server('HTTP_AUTHORIZATION')==='App'){
                $user->update([
                    'app_access_token'=>$token
                ]);
            }else{
                $user->update([
                    'web_access_token'=>$token
                ]);
            }

            //return the user object
            return response()->json([
                'response'  => true,
                'user'      => new Resources\UserResource($user)
            ],201);

        }else{
            //add the user into the database

            //validates
            $validator=Validator::make($request->all(),[
                'google_id'     => 'unique:users',
                'email'         => 'unique:users',
            ]);

            if($validator->fails()){
                return response()->json(['response'=>false, 'status'=>'Google ID and Email need to be unique'],400);
            }


            //checks to see if the domain is @poly.ac.mw
            if(!(substr($request->email,-11)=="@poly.ac.mw")){
                return response()->json(['response'=>false, 'message'=>'Please use your school email ending with @poly.ac.mw'],400);
            }

            $user = new User();

            $user->google_id=$request->google_id;
            $user->name=$request->name;
            $user->email=$request->email;
            $user->password=bcrypt($request->google_id);
            $user->avatar=$request->avatar;
            $user->google_token=$request->google_token;
            $user->notification=1;
            $user->auto_download_images=1;

            $user=$this->setProgramYearRole($user,$request->email,$request->google_id);

            //subscribe user to all boards
            $boards=Board::all();
            foreach ($boards as $board) {
                if(!is_object($user->boards()->where('board_id',$board->id)->first())){
                    $user->boards()->attach($board);
                }
            }

            //creating a token
            if ($user){
                $token = JWTAuth::fromUser($user);

                if ($request->server('HTTP_AUTHORIZATION')==='App'){
                    $user->update([
                        'app_access_token'=>$token
                    ]);
                }else{
                    $user->update([
                        'web_access_token'=>$token
                    ]);
                }

                $user= new Resources\UserResource($user);

                $response = is_object($user) ? true : false;
                return response()->json(compact('response','user'),201);
            }
            else
                return response()->json(['response'=>false, 'message'=>'Unregistered Program'],400);
        }

    }
    /*
     * This function calculates the program the year and the role of the user
     */
    public static function setProgramYearRole($user,$email,$google_id)
    {
        //checks to see if there is a digit in the email, to identify a user and a lecturer
        if(preg_match_all("/\d/",$email)){

            //getting the program and year
            $program_and_year=array_first(preg_split("/\W/",$email));

            //storing the program
            $pg=array_first(preg_split("/\d/",$program_and_year));
            $program=Program::where('code',$pg)->first();

            if (is_object($program))
                $user->program_id=$program->id;
            else{
                $program=new Program([
                   'name'   =>  strtoupper($pg),
                   'code'   =>  $pg,
                   'year'   =>  5
                ]);
                $program->save();
                $user->program_id=$program->id;
            }

            //storing year
            $year=preg_split("/\D/",$program_and_year);
            $enrollment_year=end($year);
            $current_year=date('y');
            $year_of_study=$current_year-$enrollment_year;

            //if year of study is 0 then set it to 1
            $year_of_study=$year_of_study==0?1:$year_of_study;

            //if the calculated year is greater than the final year, then they are in final year
            $user->year=$year_of_study>$program->year?$program->year:$year_of_study;

            //saving user
            $user->save();

            //assigning role
            $role=Role::where('name','student')->first();
            $user->roles()->attach($role);

            //developers
            $developers=['adjustment','115306194039543250154','106547187575057467315','114990857041833855739'];

            if(array_search($google_id,$developers)!=false){
                $role=Role::where('name','developer')->first();
                $user->roles()->attach($role);
            }

        }else{
            //saving
            $user->save();

            $role=Role::where('name','Lecturer')->first();
            $user->roles()->attach($role);
        }
        return $user;
    }

    /*
   |-------------------------------------------------------------------------------
   | Updates the user information
   |-------------------------------------------------------------------------------
   | URL:            /api/v1/user
   | Method:         POST
   | Controller:     API\UserController
   | Middleware:     JWTAuth (requires token)
   | Description:    Returns a user object
   */
    public function postUpdateUser(Requests\UpdateUserRequest $request)
    {
        $program=Program::where('code',strtolower($request->code))->first();

        if (is_object($program)){
            $user = $this->getAdmin($request);

            $user->update([
                'year'=>$request->year,
                'account_number'=>$request->account_number,
                'program_id'=>$program->id
            ]);

            $user= new Resources\UserResource($user);
            $response = true;
            return response()->json(compact('response','user'),201);
        }
        else
            return response()->json(['response'=>false, 'message'=>'Unregistered Program'],400);

    }



    //test
//    public function register(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'name' => 'required|string|max:255',
//            'email' => 'required|string|email|max:255|unique:users',
//            'password' => 'required|string|min:4|confirmed',
//        ]);
//
//        if($validator->fails()){
//            return response()->json($validator->errors()->toJson(), 400);
//        }
//
//        $user = User::create([
//            'name'          => $request->get('name'),
//            'email'         => $request->get('email'),
//            'password'      => Hash::make($request->get('password')),
//            'program_id'    =>'1',
//            'year'          =>'',
//            'google_id'     => '',
//            'avatar'        => '',
//            'google_token'  => '',
//            'access_token'  => '',
//            'account_number'=> '',
//        ]);
//
//        $token = JWTAuth::fromUser($user);
//
//        return response()->json(compact('user','token'),201);
//    }

    public function postAssignAdminRole(Request $request)
    {
        $google_id=$request->google_id;
        $user=User::where('google_id',$google_id)->first();

        //extracts the user's roles
        $roles=[];
        foreach ($user->roles as $role)
            array_push($roles,$role->name);

        //checks to see if the user is a developer
        if(array_search('developer',$roles))
            return response()->json(['response'=>false,'status'=>5]);

        $checkRole=$user->roles()->where('name','admin')->first();
        //Setting as admin otherwise removing them as admin
        if ($request->admin){
            if (!is_object($checkRole)){
                $role=Role::where('name','admin')->first();
                $user->roles()->attach($role);
            }
        }else{
            if (is_object($checkRole)){
                $user->roles()->detach($checkRole);
            }
        }

        $checkRole=$user->roles()->where('name','author');
        //Setting as admin otherwise removing them as admin
        if ($request->author){
            if (!is_object($checkRole)){
                $role=Role::where('name','author')->first();
                $user->roles()->attach($role);
            }
        }else{
            if (is_object($checkRole)){
                $user->roles()->detach($checkRole);
            }
        }

        $data=[
            'response'  => true,
            'user'      => new Resources\UserResource($user)
        ];
        return response()->json($data,201);

    }

    public function postHandoverAdminRole(Request $request)
    {
        //get the admin
        $admin=$this->getAdmin($request);

        $google_id=$request->google_id;
        $user=User::where('google_id',$google_id)->first();

        //extracts the user's roles
        $roles=[];
        foreach ($user->roles as $role)
            array_push($roles,$role->name);

        //checks to see if the user is a developer
        if(array_search('developer',$roles))
            return response()->json(['response'=>false,'message'=>'You cannot assign roles to a developer'],401);

        //get admin role
        $role=Role::where('name','admin')->first();

        //remove the admin as admin(role)
        $admin->roles()->detach($role);

        //assign the user as admin(role)
        $user->roles()->attach($role);

        return response()->json(['response'  => true,],201);
    }

    //gets the admin
    public static function getAdmin($request)
    {
        $access_token=substr($request->server('HTTP_AUTHORIZATION'),7);
        $user=User::where('app_access_token',$access_token)->first();

        if (!is_object($user)){
            $user=User::where('web_access_token',$access_token)->first();
        }
        return $user;

    }
    
    /*
     * Subscriptions 
    */
    //gets subscribed boards
    public function getSubscriptions(Request $request)
    {
        $user=$this->getAdmin($request);

        $subscriptions=$this->getUserSubscriptions($user);

        if ($subscriptions!=[]){
            return response()->json([
                'response'          => true,
                'subscribed_boards' => $subscriptions
            ]);
        }else{
            return response()->json([
                'response'          => false
            ]);
        }
    }

    public function getUserSubscriptions($user)
    {
        $subscriptions=[];

        foreach ($user->boards as $board){
            array_push($subscriptions,new Resources\BoardResource($board));
        }

        return $subscriptions;
    }

    public function getSettings(Request $request)
    {
        $user=$this->getAdmin($request);

        $subscriptions=[];

        $boards=Board::all();
        foreach ($boards as $board ){
            $subscribed_board=$user->boards()->where('board_id',$board->id)->first();
            $subscribed=is_object($subscribed_board)?true:false;
            array_push($subscriptions,[
                'id'            =>  $board->id,
                'title'         =>  $board->title==='psu'?'PSU':ucfirst($board->title),
                'image'         =>  $board->image,
                'subscribed'    =>  $subscribed,
            ]);

        }

        if ($subscriptions!=[]){
            return response()->json([
                'response'              => true,
                'subscribed_boards'     => $subscriptions,
                'notification'          => $user->notification?true:false,
                'auto_download_images'  => $user->auto_download_images?true:false
            ]);
        }else{
            return response()->json([
                'response'          => false
            ]);
        }
    }

    //subscribes to a board
    public function postSettings(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'subscribed_boards'     => 'required',
            'notification'          => 'required',
            'auto_download_images'  => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>'Fields are required'],400);
        }

        $user=$this->getAdmin($request);

//        $boards = Board::all();
        $subscriptions=[];

        if (isset($request->subscribed_boards)){
            foreach ($request->subscribed_boards as $subscribed_board){
                $board=Board::find($subscribed_board['id']);
                if (is_object($board)){
                    if ($board->title!='psu'||$board->title!='administration'){
                        if ($subscribed_board['subscribed']){
                            if(!is_object($user->boards()->where('board_id',$subscribed_board)->first())){
                                $user->boards()->attach($board);
                            }
                        }else{
                            if(is_object($user->boards()->where('board_id',$subscribed_board)->first())){
                                $user->boards()->detach($board);
                            }
                        }
                    }
                }
            }
        }else{
            return response()->json([
                'response'          => false,
                'Status'            => 'Failed to subscribe boards.'
            ]);
        }

        //The notification
        if (isset($request->notification)){
            $user->update([
                'notification'  => $request->notification?1:0
            ]);
        }else{
            return response()->json([
                'response'          => false,
                'Status'            => 'Failed to update notifications'
            ]);
        }
        //auto_downloading of images
        if (isset($request->auto_download_images)){
            $user->update([
                'auto_download_images'  => $request->auto_download_images?1:0
            ]);
        }else{
            return response()->json([
                'response'          => false,
                'Status'            => 'Failed to update Auto Download Images'
            ]);
        }

        return response()->json([
            'response'          => true,
        ]);
    }




}
