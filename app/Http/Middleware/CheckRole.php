<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()==null){
            return response(['response'=>false, 'message'=>'Access Denied'],401);
        }

        $actions=$request->route()->getAction();
        $roles=isset($actions['roles'])?$actions['roles']:null;

        $access_token=substr($request->server('HTTP_AUTHORIZATION'),7);
        $user=User::where('app_access_token',$access_token)->first();

        if (!is_object($user)){
            $user=User::where('web_access_token',$access_token)->first();
            if (!is_object($user)){
                return response(['response'=>false, 'message'=>'Access Denied1'],401);
            }
        }

        if (is_object($user)){
            if($user->hasAnyRole($roles)||!$roles){
                return $next($request);
            }
            return response(['response'=>false, 'message'=>'Access Denied.'],401);
        }

        return response(['response'=>false, 'message'=>'Access Denied'],401);
    }
}
