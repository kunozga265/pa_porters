<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoticeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:4',
            'body' => 'required|min:4',
            'featured' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'    => 'A title for the notice is required.',
            'title.min'         => 'The title should be greater than 4 characters.',
            'body.required'     => 'A body for the notice is required.',
            'body.min'          => 'The body should be greater than 4 characters.',
//            'featured.required' => 'A featured option is required.',
//            'featured.boolean'  => 'A featured option should be boolean.'
        ];
    }

}
