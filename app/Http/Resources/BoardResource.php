<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Notice;
use Mockery\Matcher\Not;

class BoardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    const AWSDOMAINNAME='https://s3.amazonaws.com/pa-porters/';

    public function toArray($request)
    {
$notices=Notice::where('board_id',$this->id)->latest()->get();

return [
    'id'        =>  $this->id,
    'title'     =>  $this->title==='psu'?'PSU':ucfirst($this->title),
    'image'     =>  $this->image,
    'notices_count'   =>  $this->notices->count()
];
}
}
