<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NoticeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    const AWSDOMAINNAME='https://s3.amazonaws.com/pa-porters/';
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'title'         =>  $this->title,
            'body'          =>  $this->body,
            'slug'          =>  $this->slug,
            'featured'      =>  $this->featured,
            'author'        =>  $this->user,
            'image'         =>  $this->image,
            'image_thumb'   =>  $this->image_thumb,
            'board'         =>  $this->board,
            'updated_at'    =>  date('jS M Y',$this->updated_at->getTimestamp()),
            'date'          =>  $this->created_at->diffForHumans(),
        ];
    }
}
