<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\API\UserController;

class PollResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $now=Carbon::now();

        $active=$now->getTimestamp()>=$this->start_date&&$now->getTimestamp()<$this->end_date?true:false;


        return [
            'id'                    =>  $this->id,
            'title'                 =>  $this->title,
            'author'                =>  $this->author,
            'type'                  =>  $this->type?'Electoral':'Generic',
            'description'           =>  $this->description,
            'options'               =>  $this->options($this->options,$this->type,$this->votes->count()),
            'voted'                 =>  $this->voted($request),
            'total_votes'           =>  $this->votes->count(),
            'start_date'            =>  $this->start_date,
            'end_date'              =>  $this->end_date,
            'end_date_readable'     =>  date('jS M Y',$this->end_date),
            'active'                =>  $active,

        ];
    }

    public function options($options,$type,$total_votes)
    {
        foreach ($options as $option){
            if ($total_votes!==0){
                $option->votes_count=round(($option->votes->count()/$total_votes)*100);
                if ($type)
                    $option->option=new UserPollResource($option->user);
                else{
                    $option->option=[
                        'name'=>$option->option
                    ];
                }

            }
            else{
                $option->votes_count=0;
                if ($type)
                    $option->option=new UserPollResource($option->user);
                else{
                    $option->option=[
                        'name'=>$option->option
                    ];
                }
            }
        }
        return $options;
    }

    public function voted($request)
    {
        $user=UserController::getAdmin($request);
        if($user->id){
            $vote=$this->votes()->where('google_id',$user->google_id)->first();

            if(is_object($vote)){
               return true;
            }
        }
        return false;
    }
}
