<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $access_token=$request->server('HTTP_AUTHORIZATION')==='App'?$this->app_access_token:$this->web_access_token;
        return [
            'id'                =>  $this->id,
            'google_id'         =>  $this->google_id,
            'name'              =>  $this->name,
            'email'             =>  $this->email,
            'program'           =>  $this->program,
            'year'              =>  $this->year,
            'account_number'    =>  $this->account_number,
            'avatar'            =>  $this->avatar,
//            'google_token'      =>  $this->google_token,
            'access_token'      =>  $access_token,
            'roles'             =>  new RoleCollection($this->roles),
        ];
    }
}
