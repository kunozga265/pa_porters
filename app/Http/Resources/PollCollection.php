<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PollCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  =>  PollResource::collection($this->collection),
//            'links'     =>  [
//                'first'             =>  $this->firstItem(),
//                'last'              =>  $this->lastItem(),
//                'prev'              =>  $this->previousPageUrl(),
//                'next'              =>  $this->nextPageUrl(),
//            ],
            'meta'      =>  [
                'current_page'      =>  $this->currentPage(),
//                'total'             =>  $this->total(),
//                'per_page'          =>  $this->perPage(),
//                'count'             =>  $this->count(),
//                'has_more_pages'    =>  $this->hasMorePages(),
                'last_page'         =>  $this->lastPage()
            ]
        ];
    }
}
