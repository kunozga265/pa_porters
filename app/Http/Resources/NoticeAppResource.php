<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NoticeAppResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    const AWSDOMAINNAME='https://s3.amazonaws.com/pa-porters/';

    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'title'         =>  $this->title,
            'body'          =>  $this->body,
            'slug'          =>  $this->slug,
            'featured'      =>  $this->featured,
            'author'        =>  $this->user,
            'image'         =>  $this->image?self::AWSDOMAINNAME.$this->image:null,
            'image_thumb'   =>  $this->image_thumb?self::AWSDOMAINNAME.$this->image_thumb:null,
            'board'         =>  [
                'id'    =>$this->board->id,
                'title' =>$this->board->title==='psu'?'PSU':ucfirst($this->board->title),
                'image' =>$this->board->image,
            ],
            'updated_at'    =>  $this->updated_at->getTimestamp(),
            'date'          =>  $this->updated_at->diffForHumans(),
        ];
    }
}
