<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserPollResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                =>  $this->id,
            'google_id'         =>  $this->google_id,
            'name'              =>  $this->name,
            'email'             =>  $this->email,
            'program'           =>  $this->program,
            'year'              =>  $this->year,
            'avatar'            =>  $this->avatar,
        ];
    }
}
