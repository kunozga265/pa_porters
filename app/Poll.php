<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    public function options()
    {
        return $this->hasMany('App\Option');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function author()
    {
        return $this->hasOne('App\User','google_id','author_id');
    }

    protected $fillable=[
        'title',
        'author_id',
        'description',
        'type',
        'start_date',
        'end_date',
    ];

    protected $hidden=[
        'updated_at',
        'created_at',
    ];
}
