<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notice extends Model
{
    use SoftDeletes;

    public function board()
    {
        return $this->belongsTo('App\Board');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected $table = 'notices';

    protected $fillable=[
        'title',
        'body',
        'board_id',
        'featured',
        'user_id',
        'image',
        'image_thumb',
        'slug',
    ];

    protected $dates=['deleted_at'];

    protected $hidden=[
        'slug',
        'title',
        'body',
        'board_id',
        'user_id',
        'image',
        'image_thumb',
        'featured',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

}
