<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    public function notices()
    {
        return $this->hasMany('App\Notice');
    }

    public function users()
    {
        return $this->belongsToMany('App\User','user_board','board_id','user_id');
    }

    protected $fillable=[
        'title',
        'image'
    ];

    protected $hidden=[
        'created_at',
        'updated_at',
        'pivot'
    ];
}
