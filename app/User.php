<?php

namespace App;

//use Laravel\Passport\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable
//        HasApiTokens
        ;

    public function notices()
    {
        return $this->hasMany('App\Notice');
    }

    public function program()
    {
        return $this->belongsTo('App\Program');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','user_role','user_id','role_id');
    }

    public function boards()
    {
        return $this->belongsToMany('App\Board','user_board','user_id','board_id');
    }

    public function hasAnyRole($roles)
    {
        if(is_array($roles)){
            foreach ($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }
        }else{
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if($this->roles()->where('name',$role)->first()){
            return true;
        }
        return false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'app_access_token',
        'web_access_token',
        'google_id',
        'name',
        'email',
        'password',
        'program_id',
        'year',
        'google_id',
        'avatar',
        'google_token',
        'account_number',
        'auto_download_images',
        'notification'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'app_access_token',
        'web_access_token',
        'email',
        'password',
        'program_id',
        'year',
        'google_token',
        'account_number',
        'notification',
        'created_at',
        'updated_at',
        'auto_download_images'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
