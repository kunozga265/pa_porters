<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function user()
    {
        return $this->hasOne('App\User','google_id','option');
    }

    public function poll()
    {
        return $this->belongsTo('App\Poll');
    }

    protected $fillable=[
        'poll_id',
        'option'
    ];

    protected $hidden=[
        'poll_id',
        'user',
        'votes',
        'updated_at',
        'created_at',
    ];
}
